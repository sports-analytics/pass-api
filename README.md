# PASS-API

Resource for accessing the Hidden ESPN API for a variety of sports analytics research and data collection.
The table below summarizes what the codes can do currently. Dates in parentheses indicate dates when those
pieces will be available for use.

*Doing some independent hockey work and not getting the cleanest of data, seems to be missing quite a bit
of coordinate data. Find some documentation on an NHL API and Fox API as well. Going to develop those
farther to see if I can improvement the quality of hockey data. Note: That this data works well and one
can still learn quite a bit from it, but coordinate wise, it may be suffering.* JZ

| Sport | Box Score | Play-by-Play | Schedule | JSON | Player Stats | Misc. |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| NFL | * | * | * | * | (10/2019) | (10/2019) |
| NCAAF | * | * | * | * | (10/2019) | * Drive Parser |
| MLB | (11/2019) | (11/2019) | * | * | (11/2019) | (11/2019) |
| NHL | * | * | * | * | * | (10/2019) |
| NCAAM | (12/2019) | (12/2019) | * | * | (12/2019) | (12/2019) |
| NCAAW | (12/2019) | (12/2019) | * | * | (12/2019) | (12/2019) |
| NBA | (12/2019) | (12/2019) | * | * | (12/2019) | (12/2019) |
| Racing | (2/2020) | (2/2020) | * | * | (2/2020) | (2/2020) |

## Rules for using Repo

1. Create your own fork for editting the code. Master branch is to be managed solely by project owners
    *   This doesn't mean a forked version won't eventually become the master version, just helps
           to protect against the main code being uploaded incorrectly.
2. All commit messages should be concise and consist of only one change per commit
3. This repo or the code herein shouldn't be shared outside of group members, code can currently be
    consider as properity information
4. All code is licensed, within the group, under the GNU General Public License. You can view that license
    in the provided GPL-License.txt file provided in this repo.
5. If the code is distributed publicly, it will be licensed under the MIT License or a privately created
    license, the MIT License can be reviewed in the provided MIT-License.txt
6. When creating a fork, you **MUST** copy the GPI-License.txt into your fork, and adjust any README or
    other files with your name as a contributor/author.
7. ***ALWAYS*** properly annonate and comment your code so individuals can best under the code

## Code Bases
1. Python
2. R (in progress)

## Dependencies

If running on Windows, download and run the install-dependencies.bat file for necessary Python
modules/libraries.

A similiar script is in development for Mac and Linux Machines.

A similiar script is in development for R.

## ToDo List

* [x]  Develop NFL Class
* [x]  Develop NHL Class
* [x]  Include Penalties in NHL PlayByPlay
* [x]  Redevelop PbP Routine to be fully general
* [ ]  Develop MLB Class
* [x]  Develop NFL and CFB Drive Parser
* [ ]  Develop Basketball Classes
* [ ]  Develop Racing Classes

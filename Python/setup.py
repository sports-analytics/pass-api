import setuptools

setuptools.setup(
	name='passapi',
	version='0.5.0',
	author='Jacob Zorn',
	author_email='jzz164@psu.edu',
	description='Retrieve Sports Information from a variety of online sources. Only to be used by Penn State Sports Analytics Club Members',
	packages=setuptools.find_packages())